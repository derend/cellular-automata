#lang scribble/manual
@(require scribble/eval
          "main.rkt"
          (for-label racket
                     "main.rkt"
                     )
          )
@(define (author-email) "deren.dohoda@gmail.com")

@title[#:tag "top"]{Elementary Cellular Automata Package}
@author{@(author+email "Deren Dohoda" (author-email))}

@defmodule[cellular-automata]

@table-of-contents[]

@section{Concepts}
A one-dimensional cellular automaton is a single row of ones and zeros ("cells") 
and a rule for producing a new row from the 
current row. A one-dimensional cellular automaton is often pictured as a 
two-dimensional grid, with the starting row of binary data (represented
as black and white pixels) on top, and the
subsequent rows produced by the action of the rule placed below it. You can
think of the horizonal axis as "space" and the vertical axis as "time."

This package allows a programmer to create an infinite sequence of such rows
or to create a monochrome bitmap representation of a portion of time. It 
also grants the programmer the ability to have cellular automata interfere
with each other's cells at the edges.

@subsection{Rule Space and Rule Number}
To transform one row into the next row, a rule is needed which examines some
local set of cells in order to produce the next row's corresponding cell. 
This package refers to this set of cells as the "rule space." For instance, 
a rule space of 3 means that three cells are used to produce the new cell:

[1][2][3]

such that the state of all three cells are used to update [2]. In order for
alignment of the update rule to be unique the rule space is enforced to be odd. 

If the rule space is R, then there are 2^R permutations of the cells to 
consider, and since the output cell of each permutation can be either 1
or 0, the total number of rules for a rule space is 2^(2^R). Any number
in the interval [0, 2^(2^R) - 1] will select a unique rule. This package names
this index the "rule number."

@subsection{Cell Width}
The cell width is simply the size of space in cells.

@subsection[#:tag "void"]{The Void}
Since a rule space of, for instance, 3 requires a left and right neighbor in order
to update a cell, cells at the edge of space stare off into a void. This 
package considers space a circle such that the cell furthest to the right does
not look into the void, but instead sees the leftmost cell as its neighbor, 
and vice versa. @seclink["impose-void"]{This behavior can be adjusted.}

@subsection{Interference}
What this package offers to tinkerers is the ability for cellular automata to 
interfere with each other at their edges of space by adjoining them in the
spacial dimension. Not only can a single automaton "look at itself" due to
the circular space, an automaton can have other neighbors instead operating
under different rule numbers and even different rule spaces. 
Thus, rule 30 in 3-space
can be adjoined to rule 260 in 5-space so that rule 3-30's right cells see 
rule 5-260's left cells, and vice versa; due to the circularity of space, 
then, rule 5-260's right cells see rule 3-30's left cells, and again vice versa.
Their interactions can produce some interesting results.

@section{Using the Package}

@subsection{Making Automata}
@defproc[(make-automaton [rule-space (and/c exact? odd? positive-integer? (>/c 1))]
                        [rule-number (and/c exact? positive-integer? (between/c 0 (expt 2 (expt 2 rule-space))))]
                        [cell-width (and/c exact? positive-integer? (>/c (quotient rule-space 2)))]
                        [state (or/c (one-of/c '(one-space zero-space 
                                                 center-one center-zero
                                                 left-one left-zero
                                                 right-one right-zero
                                                 random))
                                     (listof (one-of/c '(1 0))))])
         automaton?]{
                    Creates an automaton with the given properties. This automaton is @italic{not} a sequence. If 
                    @racket[state] is a list then its length must be equal to @racket[cell-width]. The quoted
                    values for state are convenient ways to make a centered 1 surrounded by zeros, 0 surrounded
                    by ones, or a left- or right-adjusted 1 or 0 (the remaining cells are opposite). 
                    @racket['random] uses the built-in @racket[random].}

@defproc[(automaton? [val any/c])
         boolean?]{Returns @racket[#t] if the argment is an automaton.}
@deftogether[(@defproc[(zero-automaton [cell-width (and/c exact? positive-integer?)])
                       automaton?]
               @defproc[(one-automaton [cell-width (and/c exact? positive-integer?)])
                        automaton?])]{Creates an automaton of the given @racket[cell-width] which always produces a 
                                      0 or 1, respectively. For the intended use of @racket[zero-automaton] 
                                      and @racket[one-automaton], please see @secref["impose-void"].}

@subsection{Automata Sequences}
A single form is provided to generate a sequence.
@defproc[(in-automata [v automaton?] ...) sequence?]{Creates an infinite sequence whose terms are a list of 
                                                      ones and zeros corresponding to the state of the 
                                                      included automata. Due to the fact that rule spaces must be able
                                                      to access enough cells, valid automata created by 
                                                      @racket[make-automaton] may still cause an error in @racket[in-automata] 
                                                      if an automaton's @racket[cell-width] is not large enough for its neighbors.
                                                      See @secref["void"].}

@subsection{Bitmaps}
@defproc[(automata->bitmap [auto sequence?]
                           [time (and/c positive-integer? (>/c 0))])
         bitmap?]{Creates a monochrome bitmap where a 1 is a black pixel and a 0 is a white pixel.}
@defproc[(examine-range [rule-space (and/c exact? odd? positive-integer? (>/c 1))]
                        [start (and/c exact? integer? (>=/c 0))]
                        [finish (and/c exact? integer? (>=/c 0))]
                        [#:width width (and/c exact? positive-integer?) 10]
                        [#:cell-width cell-width (and/c exact? positive-integer?) 101]
                        [#:state state (or/c (one-of/c '(one-space zero-space 
                                                            center-one center-zero
                                                            left-one left-zero
                                                            right-one right-zero
                                                            random))
                                     (listof (one-of/c '(1 0)))) 'center-one]
                        [#:the-void the-void (one-of/c '(zero one torus)) 'torus])
         bitmap?]{Creates a large bitmap made of all the automata between @racket[start] and @racket[finish] inclusive, labeled
                  by rule number and arranged in rows of size @racket[width].
                  Use @racket[the-void] to avoid wrapping space, instead bringing in a 0 or 1. 
                  Other arguments are like @racket[make-automaton], except that the resulting automata all run for 
                  a time which is also limited by @racket[cell-width].}

@section{Examples}
@subsection{Simple Use}
@(define this-eval
          (let ([eval  (make-base-eval)])
            (eval '(begin
                     (require ;racket/draw 
                              "main.rkt")))
            eval))
@examples[#:eval this-eval
                 (make-automaton 3 22 101 'zero-space)
                 (define rule-3-22 (make-automaton 3 22 101 'zero-space))
                 (define rule-5-358 (make-automaton 5 358 101 'center-one))
                 (define rule-3-30 (make-automaton 3 30 101 'random))
                 (automata->bitmap 
                  (in-automata rule-3-22 rule-5-358 rule-3-30)
                  303)
                 (automata->bitmap 
                  (in-automata rule-5-358 rule-3-22 rule-3-30)
                  303)]
@subsection[#:tag "impose-void"]{Imposing the Void}
Any rule space has a zero (or one) automaton; that is, an automaton that always produces
a white (respectively black) pixel regardless of the input. To simplify their creation some
default automata constructions are provided. These automata can be used to effectively stop
the wrapping of space at the edges.
@examples[#:eval this-eval
                 (define rule-110 (make-automaton 3 110 200 'center-one))
                 (define zero (zero-automaton 1))
                 (automata->bitmap
                  (in-automata rule-110) 400)
                 (automata->bitmap
                  (in-automata zero
                               rule-110
                               zero)
                  400)]

@subsection[#:tag "examine-range"]{Examining a Range of Automata}
@examples[#:eval this-eval
                 (examine-range 3 20 24 #:width 3)
                 (examine-range 3 20 24 #:width 3 #:the-void 'zero)
                 (examine-range 3 20 24 #:width 3 #:the-void 'one)]